# Frontend для сервиса прогнозироования цены на недвижимость в Перми

## Ссылки
- [Приложение](http://185.104.114.95)

## Инструкция по использованию приложения

Приложение представляет собой карту, с которой пользователь может взаимодействовать с помощью кнопок в верхнем левом углу.

- При клике на кнопку с изображением шестиугольника отображаются районы г. Перми, при повторном клике районы скрываются, если нажать на район правой кнопкой мыши, то отобразится его название
- При клике на соседнюю кнопку с изображением диаграммы отображаются квартиры, при повторном клике квартиры скрываются. Отображение квартир зависит от масштаба, если отдалиться, то будет отображаться тепловая карта, если приблизиться, то будут отображаться круги - дома, в которых продаются квартире. На круг можно нажать левой кнопкой мыши, чтобы получить информацию о дома и квартирах
- При клике на кнопку с изображением дома можно выбрать место, для которого нужно опредить стоимость. Для этого щелкаем на кнопку, а затем на карту, дом должен поставиться на месте курсора. Для изменения позиции дома нужно снова будет нажать на кнопк
- При клике на кнопку с настройками будут показаны характеристики дома и квартиры, которые нужно задать вручную, при повторном клике характеристики скрываются. После задания характеристик необходимо нажать на кнопку "Рассчитать стоимость", чтобы получить прогноз модели

Если поставленный дом находится за пределами границ г. Перми, то пользователь получает ошибку

## Использование внешнего API

### Overpass Turbo API

Для получения границ районов Перми используется следующий запрос:
```overpass-turbo
[out:json][timeout:25];
relation["boundary"="administrative"]["admin_level" = "9"]["type" = "boundary"]({{bbox}});
out geom;
```

### Maptiler

Для использования подложки для карты используется сервис Maptiler, в котором можно получить токен и настроить под себя карту

Токен нужно указать в переменных окружения:
```env
VITE_BASE_MAP_URL=https://api.maptiler.com/maps/<maps-id>/style.json?key=<key>
```

## Запуск

### Переменные окружения для разработки

```env
VITE_BASE_MAP_URL=<YOUR-MAP-WITH-TOKEN>
VITE_MODE=dev
VITE_API_HOST=http://127.0.0.1:8000
```

### Переменные окружения для сборки

```env
VITE_BASE_MAP_URL=<YOUR-MAP-WITH-TOKEN>
VITE_MODE=production
```
